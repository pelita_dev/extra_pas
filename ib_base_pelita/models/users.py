# -*- coding: utf-8 -*-
# © 2017 Ibrohim Binladin | ibradiiin@gmail.com | +62-838-7190-9782
from odoo import api, fields, models, _
from odoo.osv import expression
import logging
_logger = logging.getLogger(__name__)


class SaleDivision(models.Model):
    _name = 'sale.division'

    name = fields.Char(string='Division', required=True)
    code = fields.Char(string='Code')
    active = fields.Boolean(string='Status', default=True,
                            help="Set active to false to hide the tax without removing it.")

    @api.model
    def name_search(self, name, args=None, operator='ilike', limit=100):
        args = args or []
        domain = []
        if name:
            domain = ['|', ('code', '=ilike', name + '%'), ('name', operator, '%' + name + '%')]
            if operator in expression.NEGATIVE_TERM_OPERATORS:
                domain = ['&', '!'] + domain[1:]
        divisi = self.search(domain + args, limit=limit)
        return divisi.name_get()

    @api.multi
    @api.depends('name', 'code')
    def name_get(self):
        result = []
        for divisi in self:
            name = "%s" % (str(divisi.name) or _(''))
            if divisi.code:
                name = "%s" % (_("[" + str(divisi.code) + "] " + str(divisi.name)) or _(''))
            result.append((divisi.id, name))
        return result


class UserManagement(models.Model):
    _inherit = 'res.users'

    business_unit_ids = fields.Many2many('pelita.business.unit', 'res_users_business_unit_rel', 'user_id', 'business_unit_id', string='Business Unit', copy=False)
    main_business_unit = fields.Many2one('pelita.business.unit', 'Main Business Unit')
    # division_id = fields.Many2one('sale.division', related='partner_id.division_id',
    #                               string='Division', store=False, readonly=True, copy=False)

