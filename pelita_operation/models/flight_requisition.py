# -*- coding: utf-8 -*-
from odoo import fields, models,api, _
# import pytz
# from odoo.addons.mail.models.mail_template import format_tz
# from datetime import date, datetime, timedelta
# from odoo.exceptions import UserError, AccessError
import logging
_logger = logging.getLogger(__name__)

class FlightRequisition(models.Model):
	_name = 'flight.requisition'
	name = fields.Char('Number')
	date_request = fields.Date('Date Request')
	destination = fields.Many2one('area.operation','To')
	date_from = fields.Date('Date of Flight(From)')
	date_to = fields.Date('Date of Flight (To)')
	etd = fields.Datetime('ETD')
	aircraft_id = fields.Many2one('aircraft.acquisition', 'Aircraft', required=True,
            readonly=True)
	customer_id = fields.Many2one('res.partner', string='Customer', domain=[('customer','=',True)],
	 readonly=True)
    
	#customer_id = fields.Many2one('res.partner','Customer')
	base_operation_id = fields.Many2one('base.operation','Base Operation')
	creator_id = fields.Many2one('res.users','Creator')
	route_operation_ids = fields.One2many('base.operation','route_id','Route')
	state = fields.Selection([('draft', 'Draft'),('validated', 'Validated'),('cancel', 'Cancelled')],
            string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')
	# sale_order_id = fields.Many2one('sale.order', related='order_line_id.order_id', string='Sales Order', store=False,
     #                           readonly=True)


class BaseRouteOperation(models.Model):
	_inherit = 'base.operation'
	route_id = fields.Many2one('flight.requisition')
