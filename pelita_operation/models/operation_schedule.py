# -*- coding: utf-8 -*-
import pytz
#import datetime
from odoo.addons.mail.models.mail_template import format_tz
from odoo import fields, models,api, _
from datetime import date, datetime, timedelta
from odoo.exceptions import UserError, AccessError

class ScheduleType(models.Model):
	_name = 'schedule.type'
	name = fields.Char('Schedule Type')

class FlightSchedule(models.Model):
	_name = 'flight.schedule'
	_inherit = ['mail.thread', 'ir.needaction_mixin']
	_order = 'id desc, name desc'

	name = fields.Char('Flight Number')
	fl_acquisition_id = fields.Many2one('aircraft.acquisition', string="Registration Number",
                track_visibility='onchange', copy=False)
	flight_order_no = fields.Char('Flight Order Number')
	base_operation_id = fields.Many2one('base.operation', 'Base Operation')
	date_schedule = fields.Date('Date')
	aircraft_id = fields.Many2one('aircraft.aircraft','Aircraft Name', 
		related='fl_acquisition_id.aircraft_name')
	type_aircraft = fields.Char('Aircraft Type')
	etd = fields.Datetime('ETD (local time)', required=True)
	eta = fields.Datetime('ETA (local time)', required=True)
	customer_id = fields.Many2one('res.partner', string='Customer')
	#schedule_commercial = fields.Selection([('scheduled','Scheduled Commercial'),
	#	('nonscheduled','Non Scheduled Commercial')],'Schedule Commercial')
	schedule_commercial_id = fields.Many2one('schedule.commercial','Schedule Commercial')

	flight_category = fields.Selection([('domestic','Domestic'),
		('international','International')],'Flight Category')
	flight_type = fields.Selection([('commercial','Commercial'),
		('noncommercial','Non-Commercial')], string='Flight Type')
	internal_flight_type_id = fields.Many2one('internal.flight.type','Internal Flight Type')
	crew_standby_ids = fields.One2many('hr.crews','crew_stb_id','Crew Stand by',
		track_visibility='onchange',)
	crew_assignment_ids = fields.One2many('hr.crews','crew_assign_id','Crew Assignment',
		track_visibility='onchange',)
	is_standby = fields.Boolean('Stand By')
	route_ids = fields.One2many('route.flight.operation','schedule_id')
	state = fields.Selection([('draft', 'Draft'),('validated', 'Validated'),('cancel', 'Cancelled'),
		('setcrew','Crew Set')],
            string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')

	base_operation_id = fields.Many2one('base.operation', 'Base Operation', required=True)
	customer_id = fields.Many2one('res.partner', string='Customer', domain=[('customer','=',True)],
		track_visibility='onchange', required=True)
	fl_acquisition_id = fields.Many2one('aircraft.acquisition', string="Registration Number",
		track_visibility='onchange', copy=False)
	# sale_order_id = fields.Many2one('sale.order','Sales Order')
	start_date = fields.Datetime(string='Start')
	finish_date = fields.Datetime(string='Finish')
	regulation_id = fields.Many2one('regulation.regulation', 'Regulation', required=True)
	fl_hours_price_id = fields.Many2one('flight.hours.price','Flight Hours Price')

	_sql_constraints = [
		('fs_no_company_uniq', 'unique (flight_order_no)', 'Flight Order Number must be unique!'),
		('fs_date_greater', 'check(eta > etd)', 'Error! \nETD must be lower than ETA.'),
	]

	@api.onchange('fl_acquisition_id') # if these fields are changed, call method
	def change_type_aircraft(self):
		if self.fl_acquisition_id.category == 'fixedwing':
			self.type_aircraft = 'Fixed Wing'
		else:
			self.type_aircraft = 'Rotary'
	
	@api.multi
	def action_set_to_draft(self):
		return self.write({'state': 'draft'})

	@api.multi
	def action_validate(self):
		fml = self.env['flight.maintenance.log']
		for schedule in self:
			if schedule.fl_acquisition_id.category == 'rotary':
				mtr = self.env['maintenance.rotary'].search([('rotary_id','=',fml.id)])
				for item in schedule.route_ids:
					ml = fml.create({
						'fl_acquisition_id' : schedule.fl_acquisition_id.id,
                    	'flight_schedule_id':  schedule.id,
                    	'flight_number' : schedule.name,
                    	'flight_order_number' : schedule.flight_order_no,
                    	'flight_category' : schedule.flight_category,
                    	'flight_type' : schedule.flight_type,
                    	'internal_flight_type_id' : schedule.internal_flight_type_id.id,
                    	'schedule_commercial_id' : schedule.schedule_commercial_id.id,
                    	'schedule_date' : schedule.date_schedule,
                    	'etd' : schedule.etd,
                    	'eta' : schedule.eta,
                    	'customer_id' : schedule.customer_id.id,
                    	'flight_category' : schedule.flight_category,
                    	'regulation_id' : schedule.regulation_id.id,
                    	'fl_hours_price_id' : schedule.fl_hours_price_id.id,
                    	'maintenance_rotary_ids': [(0,0,{
                    	'from_id' : item.from_area_id.id,
                    	'to_id': item.to_area_id.id})],
                	})
					mtr.create({'rotary_route_ids':[(0,0,{'from_route': item.from_area_id.id,
													'to_route' : item.to_area_id.id,
													})]})
												
			else:
				for item in schedule.route_ids:
					ml = fml.create({
						'fl_acquisition_id' : schedule.fl_acquisition_id.id,
                    	'flight_schedule_id':  schedule.id,
                    	'flight_number' : schedule.name,
                    	'flight_category' : schedule.flight_category,
                    	'flight_type' : schedule.flight_type,
                    	'internal_flight_type_id' : schedule.internal_flight_type_id.id,		
                    	'schedule_commercial_id' : schedule.schedule_commercial_id.id,
                    	'schedule_date' : schedule.date_schedule,
                    	'etd' : schedule.etd,
                    	'eta' : schedule.eta,
                    	'customer_id' : schedule.customer_id.id,
                    	'flight_category' : schedule.flight_category,
                    	'regulation_id' : schedule.regulation_id.id,
                    	'fl_hours_price_id' : schedule.fl_hours_price_id.id,
                    	'maintenance_fixed_ids': [(0,0,{
                    	'from_id' : item.from_area_id.id,
                    	'to_id': item.to_area_id.id,
                    	'customer_id': item.customer_id.id})],
                	})
			self.write({'state': 'validated'})
		return True



	@api.multi
	def action_crew_schedule(self):
		for schedule in self:
			fml = self.env['flight.maintenance.log'].search([('flight_schedule_id', '=', schedule.id)])
			cs = self.env['crew.schedule']
			regulasi_jam = self.regulation_id.hour_per_day
			regulasi_week = self.regulation_id.hour_per_week
			etd = datetime.strptime(schedule.etd,'%Y-%m-%d %H:%M:%S')+timedelta(hours=7)
			eta = datetime.strptime(schedule.eta,'%Y-%m-%d %H:%M:%S')+timedelta(hours=7)
			etd_date = etd.date()
			eta_date = eta.date()
			etd_time = etd.time()
			eta_time = eta.time()
			max_hours = datetime.strptime('23:59','%H:%M')
			selisih_jam = max_hours - timedelta(hours=regulasi_jam)
			selisih_jam = selisih_jam.time()
			print "selisih jam ...", selisih_jam
			print 'eta time....',etd_time
			#jam_reg_hari = datetime.strptime(str(regulasi_jam),'%H:%M').time()
			jam_reg_hari = '{0:02.0f}:{1:02.0f}'.format(*divmod(regulasi_jam * 60, 60))
			print "dedded...", jam_reg_hari
			if etd_date != eta_date:
				raise UserError(_('Tanggal ETD dan ETA Berbeda'))
			
			selisih = eta - etd
			#selisih = selisih.time()
			#selisih =selisih.hour + time_in.minute/60.0
			total_sec = selisih.seconds
			selisih = total_sec/3600
			print "selisih....", selisih
			if selisih > regulasi_jam:
				raise UserError(_('Flight Hours over Limit'))
						
			if fml.aircraft_type == 'rotary':
				mtr = self.env['maintenance.rotary'].search([('rotary_id','=',fml.id)])
				for dt in schedule.route_ids:
					print "roouuttee...", dt.from_area_id.name
					print "mtr.....", mtr
					mtr.write({'rotary_route_ids':[(0,0,{'from_route': dt.from_area_id.id,
													'to_route' : dt.to_area_id.id,
													'customer_id': dt.customer_id.id,
													})]})
					
				for item in schedule.crew_assignment_ids:
					cv = self.env['hr.cv'].search([('employee_id','=',item.crew_id.id)])
					sekarang = datetime.today().strftime('%Y-%m-%d')
					for dt in item.crew_id:
						if selisih > regulasi_jam:
							raise UserError(_('Flight Hours over Limit'))
						mtr.write({'rotary_crew_ids':[(0,0,{'crew_id': item.crew_id.id,
							'crew_type' : 'Rotary Wing',
							'qualification': item.crew_id.qualification_id.name})]})
						cs.create({'name': 'Flight Duty',
							'employee_id' : item.crew_id.id,
							'date_from' : schedule.etd,
							'date_to' : schedule.eta,
							'flight_type': schedule.flight_type,
							'customer_id' : schedule.customer_id.id,
							'aircraft_id': schedule.fl_acquisition_id.aircraft_name.id,
							'aircraft_type' : schedule.type_aircraft,
							'etd' : schedule.etd,
							'eta' : schedule.eta})
							
				# for item in schedule.crew_standby_ids:
				# 	cv = self.env['hr.cv'].search([('employee_id','=',item.crew_id.id)])
				# 	sekarang = datetime.today().strftime('%Y-%m-%d')
				# 	for dt in item.employee_id:
				# 		if selisih > regulasi_jam:
				# 			raise UserError(_('Flight Hours over Limit atau data lain tidak match'))
				# 		mtr.write({'rotary_crew_ids':[(0,0,{'crew_id': item.crew_id.id,
				# 			'crew_type' : 'Rotary Wing',
				# 			'qualification': item.crew_id.qualification_id.name})]})
				# 		cs.create({'name': 'Flight Duty',
				# 			'employee_id' : item.crew_id.id,
				# 			'date_from' : schedule.etd,
				# 			'date_to' : schedule.eta,
				# 			'flight_type': schedule.flight_type,
				# 			'customer_id' : schedule.customer_id.id,
				# 			'aircraft_id': schedule.fl_acquisition_id.aircraft_name.id,
				# 			'aircraft_type' : schedule.type_aircraft,
				# 			'etd' : schedule.etd,
				# 			'eta' : schedule.eta})
							  			
			else:
				mfw = self.env['maintenance.fixedwing'].search([('fixedwing_id','=',fml.id)])
				fml = self.env['flight.maintenance.log']
				for item in schedule.crew_assignment_ids:
					cv = self.env['hr.cv'].search([('employee_id','=',item.crew_id.id)])
					sekarang = datetime.today().strftime('%Y-%m-%d')
					for dt in item.crew_id:
						if selisih > regulasi_jam:
							raise UserError(_('Flight Hours over Limit'))
						mfw.write({'fixed_crew_ids':[(0,0,{'crew_id': item.crew_id.id,
							'crew_type' : 'Fixed Wing',
							'qualification': item.crew_id.qualification_id.name})]})
						cs.create({'name': 'Flight Duty',
							'employee_id' : item.crew_id.id,
							'date_from' : schedule.etd,
							'date_to' : schedule.eta,
							'flight_type': schedule.flight_type,
							'customer_id' : schedule.customer_id.id,
							'aircraft_id': schedule.fl_acquisition_id.aircraft_name.id,
							'aircraft_type' : schedule.type_aircraft,
							'etd' : schedule.etd,
							'eta' : schedule.eta})
							  				
				# for item in schedule.crew_standby_ids:
				# 	cv = self.env['hr.cv'].search([('employee_id','=',item.crew_id.id)])
				# 	sekarang = datetime.today().strftime('%Y-%m-%d')
				# 	for dt in item.crew_id:
				# 		if selisih > regulasi_jam:
				# 			raise UserError(_('Flight Hours over Limit'))
				# 		mfw.write({'fixed_crew_ids':[(0,0,{'crew_id': item.crew_id.id,
				# 			'crew_type' : 'Fixed Wing',
				# 			'qualification': item.crew_id.qualification_id.name})]})
				# 		cs.create({'name': 'Flight Duty',
				# 			'employee_id' : item.crew_id.id,
				# 			'date_from' : schedule.etd,
				# 			'date_to' : schedule.eta,
				# 			'flight_type': schedule.flight_type,
				# 			'customer_id' : schedule.customer_id.id,
				# 			'aircraft_id': schedule.fl_acquisition_id.aircraft_name.id,
				# 			'aircraft_type' : schedule.type_aircraft,
				# 			'etd' : schedule.etd,
				# 			'eta' : schedule.eta,
				# 			})
							
			self.write({'state': 'setcrew'})
		return True

	
class ScheduleCommercial(models.Model):
	_name = 'schedule.commercial'
	name = fields.Char('Name')

class RouteFlightOperation(models.Model):
	_name = 'route.flight.operation'
	schedule_id = fields.Many2one('flight.schedule')
	route_id = fields.Many2one('route.operation')
	from_route = fields.Char('From', related='route_id.from_route_id.name')
	to_route = fields.Char('To', related='route_id.to_route_id.name')
	distance_nm = fields.Float('Distance (NM)', related='route_id.distance_nm')
	distance_km = fields.Float('Distance (KM)', related='route_id.distance_km')
	customer_id = fields.Many2one('res.partner', string='Customer', 
		domain=[('customer','=',True)], track_visibility='onchange')
    

class InternalFlightType(models.Model):
	_name = 'internal.flight.type'
	name = fields.Char('Name')

class HrCrews(models.Model):
	_name = 'hr.crews'
	employee_id = fields.Many2one('hr.employee',string='Crew', track_visibility='onchange')
	crew_id = fields.Many2one('hr.employee', string='crew', 
		track_visibility='onchange')
	crew_stb_id = fields.Many2one('flight.schedule','Crew Stand By')
	crew_assign_id = fields.Many2one('flight.schedule','Crew Assigntment')
	crew_type = fields.Char('Crew Type',track_visibility='onchange', compute='_compute_type',
			readonly=True)
	category = fields.Char('category')
	qualification = fields.Char('Qualification', compute='_compute_type')
	is_standby = fields.Boolean('Crew StandBy')
	

	@api.multi
	@api.onchange('crew_assign_id')
	def onchange_crew(self):
		domain = {}
		list_crew = []
		sekarang = datetime.today().strftime('%Y-%m-%d')
		cv = self.env['hr.cv'].search([])
		# for rec in cv:
		# 	if rec.employee_id.crew_categ == self.crew_assign_id.fl_acquisition_id.category or \
		# 		rec.employee_id.crew_categ == self.crew_stb_id.fl_acquisition_id.category:
		# 		for rating in rec.employee_id.rating_ids:
		# 			if rating.aircraft_id.id == self.crew_assign_id.fl_acquisition_id.aircraft_name.id or \
		# 			rec.employee_id.crew_categ == self.crew_stb_id.fl_acquisition_id.category:
		# 				for item in rec.train_ids:
		# 					if item.valid_to > sekarang:
		# 						for doc in rec.document_ids:
		# 							if doc.next_due > sekarang:
		# 								list_crew.append(rec.employee_id.id)
		for rec in cv:
			if rec.employee_id.crew_categ == self.crew_assign_id.fl_acquisition_id.category:
				for rating in rec.employee_id.rating_ids:
					if rating.aircraft_id.id == self.crew_assign_id.fl_acquisition_id.aircraft_name.id:
						for item in rec.train_ids:
							if item.valid_to > sekarang:
								for doc in rec.document_ids:
									if doc.next_due > sekarang:
										list_crew.append(rec.employee_id.id)
		domain = {'crew_id': [('id', 'in', list_crew)]}
		return {'domain' : domain}


	@api.depends('crew_id')
	def _compute_type(self):
		for record in self:
			record.qualification = record.crew_id.qualification_id.name
			if record.crew_id.crew_categ == 'fixedwing':
				record.crew_type = 'Fixed Wing'
			elif record.crew_id.crew_categ == 'rotary':
			 	record.crew_type = 'Rotary Wing'

	@api.multi
	@api.onchange('crew_id') # if these fields are changed, call method
	def _change_crew_type(self):
		if self.crew_id:
			self.qualification = self.crew_id.qualification_id.name
			if self.crew_id.crew_categ == 'fixedwing':
				self.crew_type = 'Fixed Wing'
			if self.crew_id.crew_categ == 'rotary':
			 	self.crew_type = 'Rotary Wing'

class CrewType(models.Model):
	_name = 'crew.type'
	name = fields.Char('Crew Type')

class FlightAttendant(models.Model):
	_name = 'flight.attendant'
	fixwing_id = fields.Many2one('maintenance.fixedwing')
	rotary_id = fields.Many2one('maintenance.rotary')
	crew_id = fields.Many2one('hr.employee','Flight Attendant/Other Crew')
	crew_type_id = fields.Many2one('crew.type','Crew Type')
	qualification = fields.Char('Qualification')

class ReasonReason(models.Model):
	_name = 'reason.reason'
	name = fields.Char('Name')

	
