# -*- coding: utf-8 -*-

from odoo import models, fields, api

class partner_pelita(models.Model):
	_inherit = 'res.partner'

	npwp = fields.Char(string="NPWP", size=15, required=True)
	# SOLD TO PARTY INFORMATION A
	soldtp_no = fields.Char("No. Sold to Party", size=6)
	soldtp_industrytype = fields.Char("Jenis Industry", size=40)
	soldtp_pajak = fields.Selection([
					('soldtp_ppn', "PPN"),
					('soldtp_pph22', "PPh22"),
					('soldtp_pbbkb', "PBBKB(%)")
			], string="Pajak Dikenakan")
	soldtp_producttype = fields.Selection([
					('soldtp_minyaktanah', "Minyak Tanah"),
					('soldtp_premiun', "Premium"),
					('soldtp_pertamax', "Pertamax"),
					('soldtp_pertamaxplus', "Pertamax Plus"),
					('soldtp_pertaminadex', "Pertamina Dex"),
					('soldtp_minyaksolar', "Minyak Solar"),
					('soldtp_minyakdiesel', "Minyak Diesel"),
					('soldtp_minyakbakar', "Minyak Bakar"),
					('soldtp_pelumnas', "Pelumas"),
					('soldtp_elpiji', "Elpiji 3kg / 12 kg"),
					('soldtp_avtur', "Avtur / Avgas"),
					('soldtp_petkim', "Petkim / Aspal"),
			], string="Jenis Produk")
	soldtp_angkutan = fields.Selection([
					('soldtp_angkutsendiri', "Angkut Sendiri"),
					('soldtp_jasatrans', "Jasa Transportir"),
			], string="Sistem Angkutan")
	soldtp_pembayaran = fields.Selection([
					('soldtp_cash', "Cash"),
					('soldtp_prepayment', "Pre Payment"),
					('soldtp_credit', "Kredit"),
			], string="Sistem Pembayaran")

	# Initial Data
	soldtp_salesorg = fields.Char("Sales Organization", size=4)
	soldtp_distrchannel = fields.Char("Distribution Channel", size=2)
	soldtp_division = fields.Char("Division", size=2)
	soldtp_industrycode = fields.Char("Industry Code", size=6)
	
	# Sales Order
	soldtp_saleoffice = fields.Char("Sales Office", size=4)
	soldtp_salegroup = fields.Char("Sales Group", size=3)
	soldtp_custgroup = fields.Char("Customer Group", size=2)

	# Pricing / Statistic
	soldtp_pricegroup = fields.Char("Price Group", size=2)
	soldtp_pricelist = fields.Char("Price List Type", size=2)
	soldtp_currency = fields.Char("Currency", size=3)
	soldtp_foreigncurrency = fields.Char("Foreign Currency", size=3)

	# Shipping
	soldtp_deliveryplant = fields.Char("Delivery Plant", size=4)

	# Delivery And Payment Terms
	soldtp_termpayment = fields.Char("Terms of Payment", size=4)
	soldtp_invlistdate = fields.Char("Invoicing List Dates", size=2)
	soldtp_incoterm = fields.Char("Incoterm", size=3)

	# Klasifikasi Customer
	soldtp_companytype = fields.Selection([
				('soldtp_swasta', "Swasta"),
				('soldtp_ptmn', "Anak Perusahaan PTMN"),
				('soldtp_bumn', "BUMN"),
				('soldtp_pemerintah', "Instansi Pemerintah/TNI/Polri"),
			], string="Jenis Perusahaan")

	# Kontrak
	soldtp_kontrak = fields.Selection([
				(0, "Ya"),
				(1, "Tidak"),
		], string="Terikat Kontrak")
	soldtp_jangkawaktu_start = fields.Date("Jangka Waktu Start")
	soldtp_jangkawaktu_end = fields.Date("Jangka Waktu End")
	soldtp_pemberlakuanharga = fields.Selection([
				(0, "Pembayaran"),
				(1, "Penyerahan"),
		], string="Pemberlakuan Harga")

	# Company Code Data
	soldtp_reconaccount = fields.Char("Recon Account", size=10)
	# soldtp_alamattambahan = fields.Text("Tambah Alamat")


	# SHIP TO PARTY INFORMATION B
	shiptp_no = fields.Char(related='soldtp_no', readonly=True)
	shiptp_pajak = fields.Selection(related='soldtp_pajak', readonly=True)
	shiptp_producttype = fields.Selection(related='soldtp_producttype', readonly=True)

	# Initial Data
	shiptp_salesorg = fields.Char(related='soldtp_salesorg', readonly=True)
	shiptp_distrchannel = fields.Char(related='soldtp_distrchannel', readonly=True)
	shiptp_division = fields.Char(related='soldtp_division', readonly=True)
	shiptp_industrycode = fields.Char(related='soldtp_industrycode', readonly=True)
	
	# Sales Order
	shiptp_saleoffice = fields.Char(related='soldtp_saleoffice', readonly=True)
	shiptp_salegroup = fields.Char(related='soldtp_salegroup', readonly=True)
	shiptp_custgroup = fields.Char(related='soldtp_custgroup', readonly=True)

	# Pricing / Statistic
	shiptp_pricegroup = fields.Char(related='soldtp_pricegroup', readonly=True)
	shiptp_pricelist = fields.Char(related='soldtp_pricelist', readonly=True)
	shiptp_currency = fields.Char(related='soldtp_currency', readonly=True)

	# Shipping
	shiptp_deliveryplant = fields.Char(related='soldtp_deliveryplant', readonly=True)

	# Klasifikasi Customer
	shiptp_companytype = fields.Selection(related='soldtp_companytype', readonly=True)

	# Keterangan 
	shiptp_keterangan = fields.Text("Keterangan")

#soldtp_ = fields.Char("", size=)
"""
soldtp_ = fields.Selection([
				('', ""),
				('', ""),
				('', ""),
		], string="")
"""

#shiptp_ = fields.Char("", size=)
"""
shiptp_ = fields.Selection([
				('', ""),
				('', ""),
				('', ""),
		], string="")
"""