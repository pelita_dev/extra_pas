# -*- coding: utf-8 -*-
{
    'name': "Extends ResPartner",

    'description': """
        Extends ResPartner Module for Pelita Aircraft Application
    """,

    'author': "Alfie Qashwa",
    'website': "https://github.com/alfieqashwa",
    'category': 'Uncategorized',
    'version': '0.1',
    'depends': [
        'base',
        'sale',
    ],
    
    'data': ['views/views.xml'],
}